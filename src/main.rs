use watering_buddy::model::{Plant, Frequency, Room, WateringData};

fn main() {
    let plant = Plant {
        id: 0,
        name: "Geldbaum".to_string(),
        room: "Arbeitszimmer".to_string(),
        last_watered: "gestern".to_string(),
        information: Some("Ich bin ein Link".to_string()),
        comment: None,
        frequency: Frequency::Medium,
    };

    let room = Room {
        name: "Arbeitszimmer".to_string(),
    };

    let watering_data = WateringData {
        plants: vec![plant],
        rooms: vec![room],
    };

    println!("{:?}", watering_data.plants[0]);
}
