#[derive(Debug)]
pub struct Plant {
    pub id: u16,
    pub name: String,
    pub room: String,
    pub last_watered: String, // use chronos
    pub information: Option<String>,
    pub comment: Option<String>,
    pub frequency: Frequency,
}

#[derive(Debug)]
pub enum Frequency {
    VeryLow = 30,
    Low = 14,
    Medium = 7,
    High = 4,
    VeryHigh = 2,
}

#[derive(Debug)]
pub struct Room {
    pub name: String,
}

#[derive(Debug)]
pub struct WateringData {
    pub plants: Vec<Plant>,
    pub rooms: Vec<Room>,
}
